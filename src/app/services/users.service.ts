import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  base_url=environment.base_url;

  constructor(private http : HttpClient) { }

  solicitudes(){
    return this.http.get(`${this.base_url}/solicitudes`).pipe(
      
    );
  }

  aprove(data: any){
    return this.http.post(`${this.base_url}/active`,data);
  }
}

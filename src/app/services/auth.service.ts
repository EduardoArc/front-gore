import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoginForm } from '../interfaces/loginForm';
import {  tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { registerForm } from '../interfaces/registerForm';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  base_url=environment.base_url;

  isReceptor = false;
  isPostulante = false;

  constructor(private http: HttpClient, private router:Router) { }

  register(data : registerForm){
    return this.http.post(`${this.base_url}/register`, data).pipe(
      tap((res: any) => {
        console.log('res REGISTER' , res)
      })
    )
  }

  login(data: LoginForm) {
    return this.http.post(`${this.base_url}/login`, data).pipe(
      tap((res: any) => {
        console.log(res)
          localStorage.setItem('id', res.id),
          localStorage.setItem('correo', res.email),
          localStorage.setItem('nombres', res.names),
          localStorage.setItem('apellidos', res.surnames),
          localStorage.setItem('rol', res.role),
          localStorage.setItem('token-cec', res.token)


      })
    );
  }

  newPassword(passwordForm: any , token: string){
    
    const headers= new HttpHeaders()
    .set('content-type', 'application/json')
    .set('auth-token', token);

  console.log(headers); 

    return this.http.post(`${this.base_url}/newpassword`, passwordForm, { "headers" : headers }).pipe(

      
      tap((res: any) => {
        console.log('RESPUESTA')
        console.log(res)

      })

      

      
    );
    
  }

  forgot(email: any){
    return this.http.post(`${this.base_url}/forgot`, email).pipe(
      tap((res: any) => {
        console.log(res)

      })
    );
  }

  logout() {
    localStorage.removeItem('token-cec');
    localStorage.removeItem('nombres');
    localStorage.removeItem('apellidos');
    localStorage.removeItem('correo');
    localStorage.removeItem('rol');
    localStorage.removeItem('id');

    this.router.navigate(['/auth/login']);
  }


  //Veriticadores de token y tipo de usuario
  loggedIn(): boolean {
    return !!localStorage.getItem('token-cec');
  }

  handdleRole() {
    if(localStorage.getItem('rol') == 'RECEPTOR'){
      this.isReceptor = true;
    }else{
      this.isReceptor = false;
    }

    if(localStorage.getItem('rol') == 'POSTULANTE'){
      this.isPostulante = true;
    }else{
      this.isPostulante = false;
    }
  }

  
}

export interface registerForm {
    names:string,
    surnames:string,
    document :string,
    identificacion:string,
    email:string,
    phone:string,
    password:string,
    password_confirm:string,
}
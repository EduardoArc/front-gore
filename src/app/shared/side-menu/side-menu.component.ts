import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  public pageTitle?: any;
  currentRoute?: string;
  hidden = false;

  public totalSolicitudes = 0;

  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  title = 'Postulación de proyectos Gobierno Regional';

  names?: string = '';
  surNames?: string = '';
  role?: string = '';

  constructor(changeDetectorRef: ChangeDetectorRef,
               media: MediaMatcher, 
               public authService : AuthService , 
               public userService : UsersService,
               private route: ActivatedRoute, 
               private router: Router) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

    

  }

  ngOnInit(): void {
    this.getUsers();
    this.authService.handdleRole();

    this.names = localStorage.getItem('nombres')! ;
    this.surNames = localStorage.getItem('apellidos')! ;
    this.role = localStorage.getItem('rol')! ;

    this.getRouteName();
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
          // Show loading indicator
          console.log('Route change detected');

          

      }

      if (event instanceof NavigationEnd) {
          // Hide loading indicator
          this.currentRoute = event.url;          
            console.log(event);
            
            this.getRouteName();
      }

      if (event instanceof NavigationError) {
          // Hide loading indicator

          // Present error to user
          console.log(event.error);
      }
  });
   

  }

  getUsers(){
    this.userService.solicitudes().subscribe({
      next:(res : any)=>{

        if(res.length == 0){
          this.hidden = true;
        }
        
        this.totalSolicitudes = res.length;

        
        
      }
    })
  }

  

  handleLogout(){
    this.authService.logout();
  }

  profile(){
    alert('perfil')
  }

  getRouteName(){
    this.route.url.subscribe(()=>{
      console.log(this.route?.snapshot?.firstChild?.data['title'])
      this.pageTitle =  this.route?.snapshot?.firstChild?.data['title']
    });
  }

}

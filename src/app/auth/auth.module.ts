import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { SharedModule } from '../shared/shared.module';
import { RecoveryPasswordComponent } from './pages/recovery-password/recovery-password.component';
import { DialogRegisterComponent } from './components/dialog-register/dialog-register.component';






@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    RecoveryPasswordComponent,
    DialogRegisterComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,


  ]
})
export class AuthModule { }

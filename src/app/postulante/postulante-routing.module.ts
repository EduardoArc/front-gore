import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SideMenuComponent } from '../shared/side-menu/side-menu.component';
import { PostulacionesComponent } from './postulaciones/postulaciones.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'main', component: SideMenuComponent, children: [
        
          { path: 'postulaciones', component: PostulacionesComponent,  data: { breadcrumb: 'Postulaciones' , title : 'Postulaciones'} },
          { path: '',  redirectTo: 'welcome', pathMatch: 'full', data: { breadcrumb: 'Inicio' , title : 'Inicio' }  }, 
        ]
      },
      { path: '**', redirectTo: 'main', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostulanteRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostulanteRoutingModule } from './postulante-routing.module';
import { PostulacionesComponent } from './postulaciones/postulaciones.component';


@NgModule({
  declarations: [
    PostulacionesComponent
  ],
  imports: [
    CommonModule,
    PostulanteRoutingModule
  ]
})
export class PostulanteModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { CrudTestComponent } from './pages/crud-test/crud-test.component';

import { SharedModule } from '../shared/shared.module';
import { DialogAproveUserComponent } from './components/dialog-aprove-user/dialog-aprove-user.component';




@NgModule({
  declarations: [
    CrudTestComponent,
    DialogAproveUserComponent,
    
   
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    
  
    SharedModule
  ]
})
export class AdminModule { }

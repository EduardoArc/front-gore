import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { UsersService } from 'src/app/services/users.service';

import {MatDialog} from '@angular/material/dialog';
import { DialogAproveUserComponent } from '../../components/dialog-aprove-user/dialog-aprove-user.component';




@Component({
  selector: 'app-crud-test',
  templateUrl: './crud-test.component.html',
  styleUrls: ['./crud-test.component.css']
})
export class CrudTestComponent implements OnInit {

  

  displayedColumns: string[] = ['names', 'surnames', 'email', 'phone', 'actions'];
  allUsers:any[] = [];
  dataSource: MatTableDataSource<any>;

  

  constructor(private usesrService : UsersService,  public dialog: MatDialog) { 
    this.dataSource = new MatTableDataSource(this.allUsers)
  }

  ngOnInit(): void {
    this.getSolicitudes();
  }

  aproveUser(user : any){
    //console.log(user)
    this.dialog.open(DialogAproveUserComponent, {
      width: '350px',
      data: user,
    });
  }

  getSolicitudes(){
    this.usesrService.solicitudes().subscribe({
      next:(res : any)=>{
        console.log(res)
        this.dataSource = new MatTableDataSource(res);
        
      }
    })

  }

}

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { registerForm } from 'src/app/interfaces/registerForm';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-dialog-aprove-user',
  templateUrl: './dialog-aprove-user.component.html',
  styleUrls: ['./dialog-aprove-user.component.css']
})
export class DialogAproveUserComponent implements OnInit {

  user?: any = {};

  constructor(@Inject(MAT_DIALOG_DATA) public data: string,
  public userService : UsersService,
   public dialogRef: MatDialogRef<any>,) { 
    //console.log(data)
    this.user = data;
  }

  ngOnInit(): void {
  }

  onAproveClick(){
    this.userService.aprove({id : this.user.id}).subscribe({
      next:(res)=>{
        console.log(res);
        this.dialogRef.close();
        window.location.reload();
      }
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

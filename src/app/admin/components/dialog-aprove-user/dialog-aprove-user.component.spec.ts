import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAproveUserComponent } from './dialog-aprove-user.component';

describe('DialogAproveUserComponent', () => {
  let component: DialogAproveUserComponent;
  let fixture: ComponentFixture<DialogAproveUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogAproveUserComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DialogAproveUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
